# Arachnid

## Overview

Arachnid provides the Arachnid meta-packages (batteries included stacks for Arachnid) including the Hopac variant of the Arachnid stack. The Arachnid meta-package depends on parts of the Arachnid stack to give a good default install, to which other optional elements of Arachnid can be added.

This repository also serves as the main project issue tracking and communication point, as the stack is split across multiple repositories. The repositories are listed below.

## Status

TODO

## See Also

In addition to the main Arachnid site ([arachnid?.io](https://arachnid?.io)), these repositories make up parts of the Arachnid stack.

The following are included as part of the Arachnid meta-package.

* [Core](https://gitlab.com/arachnid-project/arachnid-core) - Core Model
* [Types](https://gitlab.com/arachnid-project/arachnid-types) - Types and Parsers
* [Optics](https://gitlab.com/arachnid-project/arachnid-optics) - Optics
* [Machines](https://gitlab.com/arachnid-project/arachnid-machines) - Machine Abstractions
* [Routers](https://gitlab.com/arachnid-project/arachnid-routers) - Routers

The following are not part of the Arachnid meta-package but provide useful addons (see the Arachnid documentation at [docs.arachnid.io](https://docs.arachnid.io) for more information).

* [Polyfills](https://gitlab.com/arachnid-project/arachnid-polyfills) - Polyfills for Experimental Standards Support

The following are useful when developing your own Arachnid applications for writing unit tests for Arachnid logic:

* [Testing](https://gitlab.com/arachnid-project/arachnid-testing) - Testing Utilities

## More

In addition to the main Arachnid site ([arachnid.io](https://arachnid.io)), feel free to ask any questions in the Arachnid [Gitter Room](https://gitter.im/arachnid-project/arachnid).

## Maintainers

* [@petejohanson](https://gitlab.com/petejohanson)
